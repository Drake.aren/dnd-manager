const express = require('express')
const app = express()
const datasource = require('./db.js')
datasource.setPath('../database/db.json')
const db = datasource.get()
const port = 3000

app.get('/', (req, res) => {
  res.sendFile('index.html')
})

app.post('/add/:item', (req, res) => {
  // for spell/character an so on
  db[req.params.item].push(req.body)
  datasource.write(db)
})

app.get('/list/:item', (req, res) => {
  res.json(db[req.params.item])
})

app.get('/:item/:index', (req, res) => {
  res.json(
    db[req.params.item].filter(e => {
      return e.name === req.params.index
    })[0]
  )
})

app.post('/:item/:index', (req, res) => {
  console.log(req)
  db[req.params.item].forEach(element => {
    console.log(element.name, req.params.index)
    if (element.name === req.params.index) {
      element = req.body
    }
  })
  // datasource.write(db)
})

app.listen(port, () => {
  console.log(`DnD Manager listening on port ${port}!`)
})
