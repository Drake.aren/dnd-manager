export default {
  namespaced: true,
  state: {
    history: ['Начало истории']
  },
  getters: {},
  mutations: {
    addToHostory(state, event) {
      state.history.push(event);
    }
  }
};
